import React, { Component } from 'react';
import './App.css';
import lacrima from './Mozart - Lacrimosa.mp3'
import AudioPlayer from "react-h5-audio-player";


class App extends Component {

  render() {
    return (
      <div className="App">
        <AudioPlayer
            autoPlay
            src={lacrima}
            onPlay={e => console.log("onPlay")}
            // other props here
        />
        <header className="App-header">
          <img src={'https://scontent-mxp1-1.xx.fbcdn.net/v/t1.0-9/53030435_2108434829235743_2524767624118665216_n.jpg?_nc_cat=105&_nc_ht=scontent-mxp1-1.xx&oh=f150c3e8d5a1852727f6afb1c7525121&oe=5D4FEECC'} className="App-logo" alt="logo" />
          <p style={{marginTop: '10%'}}>
            Big <code style={{color: 'red'}}><em><b>Laura</b></em></code> is watching you
          </p>
        </header>

      </div>
    );
  }
}


export default App;
